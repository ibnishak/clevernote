# Clevernote

Clevernote theme for Tiddlywiki

### For development

* Require tiddlywiki (nodejs)

``` bash
npm install
npm run start
```

### For buiding single file html

``` bash
npm install
npm run build-empty
```
